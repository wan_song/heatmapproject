package com.create.model;


/**
 * 
 * <p>标题：格网及权重对象</p>
 * <p>描述：屏幕上的点及点的权重值</p>
 * <p>Copyright：Copyright(c) 2014 founder</p>
 * <p>日期：2014-6-16</p>
 * @author	wan_song
 */
public class Reseau {
	private ScreenPoint spoint;
	private int weight=0;

	public Reseau(ScreenPoint screenPoint) {
		// TODO Auto-generated constructor stub
		this.spoint=screenPoint;
	}

	public ScreenPoint getSpoint() {
		return spoint;
	}

	public void setSpoint(ScreenPoint spoint) {
		this.spoint = spoint;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public void addWeight(int value){
		this.weight+=value;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Reseau){
			Reseau other=(Reseau) obj;
			if(other.spoint.equals(other.spoint)){
				return true;
			}
		}
		return false;
	}
	
	
}
