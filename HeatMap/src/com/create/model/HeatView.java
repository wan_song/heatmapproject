package com.create.model;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import com.create.colortable.ColorTable;
import com.create.colortable.CustomColorTable;

/**
 * <p>标题：热区展现</p>
 * <p>描述：在内存中生成热区</p>
 * <p>Copyright：Copyright(c) 2014 founder</p>
 * <p>日期：2014-6-11</p>
 * @author	wan_song
 */

public class HeatView {
	private HeatMapConfig config;
	private ColorTable colorTable=new CustomColorTable();
	public HeatView(HeatMapConfig config) {
		this.setConfig(config);
	}
	
	public HeatMapConfig getConfig() {
		return config;
	}

	public void setConfig(HeatMapConfig config) {
		this.config = config;
	}

	public BufferedImage createBufferImage(HeatSpotMode mode,int maxValue){
		Reseau[][] reseaus=mode.getReseaus();
		this.colorTable.buildColorTable(this.config,mode,maxValue,reseaus);
		BufferedImage image = new BufferedImage(mode.getColumnAmount(), mode.getRowAmount(), BufferedImage.TYPE_INT_ARGB);
		//获取画笔
		WritableRaster raster = image.getRaster();
		//开始画画
		int minvalue=config.getThreshold()*maxValue;
		for (int i = reseaus.length-1; i >=0 ; i--)
		{
			Reseau[] rs=reseaus[i];
			for(int j=rs.length-1;j>=0;j--){
					Reseau reseau=rs[j];
					if(reseau==null)
						continue;
					int weight=reseau.getWeight();
					if (weight-minvalue > 0)
					{
						int[] color;
						//获取颜色值
						color = colorTable.getARGB(weight-minvalue);
						//画像素点
						raster.setPixel(j,i, color);
					}
				}
		}
		return image;
	}
}
