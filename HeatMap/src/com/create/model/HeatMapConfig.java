package com.create.model;

import java.util.List;

import com.create.colortable.ColorLevel;

/**
 * 
 * <p>
 * 标题：热区配置
 * </p>
 * <p>
 * 描述：用于热区相关配置的参数管理
 * </p>
 * <p>
 * Copyright：Copyright(c) 2014 founder
 * </p>
 * <p>
 * 日期：2014-6-11
 * </p>
 * 
 * @author wan_song
 */
public class HeatMapConfig {
	private Extent extent;
	private int threshold;
	private boolean useLocalMaximum;
	private double radius;
	private List<ColorLevel> gradient;
	private int opacity;
	private double resolution;

	/**
	 * 
	 * <p>
	 * 说明：
	 * </p>
	 * <p>
	 * 时间：2014-6-11 下午4:14:02
	 * </p>
	 * 
	 * @author wan_song
	 * @param extent
	 *            边界值
	 * @param width
	 *            　屏宽
	 * @param height
	 *            　屏高
	 * @param useLocalMaximum
	 *            　是否以当前屏幕为标准　若否权重就会以所有数据来进行技术，若是就以当前屏幕上的点来计算权重
	 * @param radius
	 *            　度　单位为米
	 * @param gradient
	 *            渐变色
	 * @param opacity
	 *            透明度
	 * @param threshold 阀值
	 * @param resolution TODO
	 */
	public HeatMapConfig(Extent extent,
			boolean useLocalMaximum, double radius, List<ColorLevel> gradient,
			int opacity, int threshold, double resolution) {
		this.extent=extent;
		this.useLocalMaximum=useLocalMaximum;
		this.radius=radius;
		this.resolution=resolution;
		this.gradient=gradient;
		this.opacity=opacity;
		this.threshold=threshold;
	}

	public double getResolution() {
		return resolution;
	}

	public void setResolution(double resolution) {
		this.resolution = resolution;
	}

	public Extent getExtent() {
		return extent;
	}

	public void setExtent(Extent extent) {
		this.extent = extent;
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}




	public boolean isUseLocalMaximum() {
		return useLocalMaximum;
	}

	public void setUseLocalMaximum(boolean useLocalMaximum) {
		this.useLocalMaximum = useLocalMaximum;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}


	public List<ColorLevel> getGradient() {
		return gradient;
	}

	public void setGradient(List<ColorLevel> gradient) {
		this.gradient = gradient;
	}

	public int getOpacity() {
		return opacity;
	}

	public void setOpacity(int opacity) {
		this.opacity = opacity;
	}

}
