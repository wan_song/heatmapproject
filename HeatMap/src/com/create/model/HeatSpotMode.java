package com.create.model;

import java.util.List;
import java.util.TreeSet;



public class HeatSpotMode {

	Extent extent;// 范围。位于范围外的数据不作处?

	//	 单位为米
	private double resolution;// 半径为度

	private double radius;// 辐射半径 px

	private double width;

	private double height;

	private int rowAmount; //像素高度

	private int columnAmount; //像素宽度

	private Reseau[][] reseaus;

	private DefaultEasing easing;
	private TreeSet<Integer> weightRange;
	private HeatSpotMode(){
		weightRange=new TreeSet<Integer>();
	}
	public static HeatSpotMode getInstance(Extent extent, double radius, double resolution,int maxWeight){
		HeatSpotMode heatSpotMode = new HeatSpotMode();
		heatSpotMode.extent=extent;
		heatSpotMode.resolution=resolution;
		heatSpotMode.radius=radius;
		heatSpotMode.width = extent.getMaxX() - extent.getMinX();
		heatSpotMode.height = extent.getMaxY() - extent.getMinY();
		//像素大小统计
		heatSpotMode.rowAmount = (int) Math.ceil(heatSpotMode.height / resolution);
		heatSpotMode.columnAmount = (int) Math.ceil(heatSpotMode.width / resolution);
		heatSpotMode.easing=new DefaultEasing(maxWeight);
		return heatSpotMode;
	}
	public void buildModel(List<MapPoint> lonlats) {
		//清空权重分布数组
		weightRange.clear();
		/*
		 * 将所有坐标对应到网格
		 */
		this.reseaus=new Reseau[rowAmount][columnAmount];// 用来保存?有网?;
		
		//System.out.println("辐射的圈数---"+loop);
		int loop = (int) Math.ceil(this.radius);
		/*
		 * 循环现有的坐标，将坐标对应到网格中
		 */
		//专题图的四周范围多扩大一个辐射半径
		Extent bufferEnve = this.getBufferExtent(this.extent, this.resolution);
		for (int i = lonlats.size()-1; i >=0 ; i--){
			MapPoint lonlat = lonlats.get(i);
			Reseau rc = this.getReseau(lonlat);
			if (bufferEnve.contain(lonlat)){
				for (int row = 0; row <= loop; row++){
					int maxColumn = this.calculateMaxRow(row, loop);
					for (int column = 0; column <= maxColumn; column++){
						double dis = Math.sqrt(row * row + column * column);
						int value = this.easing.calculate(dis / loop,lonlat.getValue());
						this.addValue(row, column, value, rc.getSpoint().getRow(), rc.getSpoint().getColumn());
					}
				}
			}
		}
	}
	
	public long getMaxWeight(){
		long max=0;
		for(Reseau[] reseau:this.reseaus){
			for(Reseau temp:reseau){
				if(temp==null)
					continue;
				int weight=temp.getWeight();
				if(max<weight){
					max=weight;
				}
			}
		}
		return max;
	}
	/**
	 * 
	 * <p>说明：向屏幕像素点上添加权重</p>
	 * <p>时间：2014-6-12 上午11:24:10</p>
	 * @param x
	 * @param y
	 * @param value
	 * @param row
	 * @param column
	 */
	private void addValue(int x, int y, int value, int row, int column){
		if (x == 0){
			if (y == 0){
				if (row >= 0 && row < this.rowAmount && column >= 0 && column < this.columnAmount){
					this.reseaus[row][column].addWeight(value);
				}
			}else{
				if (row >= 0 && row < this.rowAmount){
					int newColumn = column + y;
					if (newColumn >= 0 && newColumn < this.columnAmount){
						if(this.reseaus[row][newColumn]==null) this.reseaus[row][newColumn]=new Reseau(new ScreenPoint(row, newColumn));
						this.reseaus[row][newColumn].addWeight(value);
					}
					newColumn = column - y;
					if (newColumn >= 0 && newColumn < this.columnAmount){
						if(this.reseaus[row][newColumn]==null) this.reseaus[row][newColumn]=new Reseau(new ScreenPoint(row, newColumn));
						this.reseaus[row][newColumn].addWeight(value);
					}
				}
			}
		}else{
			if (y == 0){
				if (column >= 0 && column < this.columnAmount){
					int newRow = row + x;
					if (newRow >= 0 && newRow < this.rowAmount){
						if(this.reseaus[newRow][column]==null) this.reseaus[newRow][column]=new Reseau(new ScreenPoint(newRow, column));
						this.reseaus[newRow][column].addWeight(value);
					}
					newRow = row - x;
					if (newRow >= 0 && newRow < this.rowAmount){
						if(this.reseaus[newRow][column]==null) this.reseaus[newRow][column]=new Reseau(new ScreenPoint(newRow, column));
						this.reseaus[newRow][column].addWeight(value);
					}
				}
			}else{
				int newRow = row + x;
				int newColumn = column + y;
				if (newRow >= 0 && newRow < this.rowAmount && newColumn >= 0 && newColumn < this.columnAmount){
					//	右下
					if(this.reseaus[newRow][newColumn]==null) this.reseaus[newRow][newColumn]=new Reseau(new ScreenPoint(newRow, newColumn));
					this.reseaus[newRow][newColumn].addWeight(value);
				}
				newRow = row + x;
				newColumn = column - y;
				if (newRow >= 0 && newRow < this.rowAmount && newColumn >= 0 && newColumn < this.columnAmount){
					//	左下
//					右下
					if(this.reseaus[newRow][newColumn]==null) this.reseaus[newRow][newColumn]=new Reseau(new ScreenPoint(newRow, newColumn));
					this.reseaus[newRow][newColumn].addWeight(value);
				}
				newRow = row - x;
				newColumn = column + y;
				if (newRow >= 0 && newRow < this.rowAmount && newColumn >= 0 && newColumn < this.columnAmount){
					//	右上
//					右下
					if(this.reseaus[newRow][newColumn]==null) this.reseaus[newRow][newColumn]=new Reseau(new ScreenPoint(newRow, newColumn));
					this.reseaus[newRow][newColumn].addWeight(value);
				}
				newRow = row - x;
				newColumn = column - y;
				if (newRow >= 0 && newRow < this.rowAmount && newColumn >= 0 && newColumn < this.columnAmount){
					//	左上
//					右下
					if(this.reseaus[newRow][newColumn]==null) this.reseaus[newRow][newColumn]=new Reseau(new ScreenPoint(newRow, newColumn));
					this.reseaus[newRow][newColumn].addWeight(value);
				}
			}
		}
	}
	/**
	 * 
	 * <p>说明：计算最大半径</p>
	 * <p>时间：2014-6-12 上午11:19:15</p>
	 * @param row
	 * @param loop
	 * @return
	 */
	private int calculateMaxRow(int row, int loop){
		return (int) (Math.ceil(Math.sqrt(loop * loop - row * row)));
	}
	/**
	 * 传经纬度，求该坐标对应的网格
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private Reseau getReseau(MapPoint lonlat){
		double width = lonlat.getX() - extent.getMinX();
		double height = extent.getMaxY() - lonlat.getY();

		int row = (int) ( height/ this.resolution);
		int column = (int) (width / this.resolution);
		Reseau temp=reseaus[row][column];
		if(temp==null)
		{
			temp=new Reseau(new ScreenPoint(row, column));
			reseaus[row][column]=temp;
		}
		return temp;
	}
	private Extent getBufferExtent(Extent enve, double radius){
		radius=radius*this.resolution;
		Extent bufferEnvelope=null;
		try {
			bufferEnvelope = enve.clone();
		} catch (CloneNotSupportedException e) {
			//....
		}
		double minX = enve.getMinX();
		double maxX = enve.getMaxX();
		double minY = enve.getMinY();
		double maxY = enve.getMaxY();
		bufferEnvelope.setMinX(minX - radius);
		bufferEnvelope.setMaxX(maxX + radius);
		bufferEnvelope.setMinY(minY - radius);
		bufferEnvelope.setMaxY(maxY + radius);
		return bufferEnvelope;
	}
	public Extent getExtent() {
		return extent;
	}
	public void setExtent(Extent extent) {
		this.extent = extent;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public int getRowAmount() {
		return rowAmount;
	}
	public void setRowAmount(int rowAmount) {
		this.rowAmount = rowAmount;
	}
	public int getColumnAmount() {
		return columnAmount;
	}
	public void setColumnAmount(int columnAmount) {
		this.columnAmount = columnAmount;
	}
	public Reseau[][] getReseaus() {
		return reseaus;
	}
	public TreeSet<Integer> getWeightRange() {
		if(weightRange.size()==0)
		for(Reseau[] rs:reseaus){
			for(Reseau r:rs){
				if(r!=null){
					weightRange.add(r.getWeight());
				}
			}
		}
		return weightRange;
	}}
