package com.create.model;

/**
 * 
 * <p>
 * 标题：地图点
 * </p>
 * <p>
 * 描述：地图点模型
 * </p>
 * <p>
 * Copyright：Copyright(c) 2014 founder
 * </p>
 * <p>
 * 日期：2014-6-11
 * </p>
 * 
 * @author wan_song
 */
public class MapPoint {
	private double x;
	private double y;
	private long value;
	public MapPoint(double x, double y, long value) {
		super();
		this.x = x;
		this.y = y;
		this.value=value;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
