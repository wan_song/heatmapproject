package com.create.model;



/**
 * 
 * <p>边界：</p>
 * <p>描述：地图的边界范围</p>
 * <p>Copyright：Copyright(c) 2014 founder</p>
 * <p>日期：2014-6-11</p>
 * @author	wan_song
 */
public class Extent {
private double minX;
private double minY;
private double maxX;
private double maxY;
/**
 * 
 * <p>说明：构建函数</p>
 * <p>时间：2014-6-11 下午4:06:23</p>
 * @author	wan_song
 * @param minx　最小x
 * @param miny  最小y
 * @param maxx  最大x
 * @param maxy  最大y
 */
public Extent(double minx, double miny, double maxx, double maxy) {
	super();
	this.minX = minx;
	this.minY = miny;
	this.maxX = maxx;
	this.maxY = maxy;
}

public Extent() {
	super();
	// TODO Auto-generated constructor stub
}

public boolean contain(MapPoint lonlat) {

	if (lonlat.getX() > this.minX && lonlat.getX() < this.maxX
			&& lonlat.getY() > this.minY && lonlat.getY() < this.maxY) {
		return true;
	} else {
		return false;
	}
}
public double getCenterX(){
	return (this.maxX-this.minX)/2.0;
}
public double getCenterY(){
	return (this.maxY-this.minY)/2.0;
}
public double getMinX() {
	return minX;
}
public void setMinX(double minX) {
	this.minX = minX;
}
public double getMinY() {
	return minY;
}
public void setMinY(double minY) {
	this.minY = minY;
}
public double getMaxX() {
	return maxX;
}
public void setMaxX(double maxX) {
	this.maxX = maxX;
}
public double getMaxY() {
	return maxY;
}
public void setMaxY(double maxY) {
	this.maxY = maxY;
}
@Override
public Extent clone() throws CloneNotSupportedException {
	// TODO Auto-generated method stub
	return new Extent(this.minX,this.minY,this.maxX,this.maxY);
}

}
