package com.create.model;



public class DefaultEasing implements Easing
{
	private int maxValue  ;
	private int NUM = 3;
	public DefaultEasing(int maxValue)
	{
		this.maxValue = maxValue;
	}
	public int calculate(double scale,long temp)
	{
		double x = scale*NUM;
		int value = (int)((Math.pow(Math.E,-1*0.5*Math.pow(x, 2)))*maxValue*temp);
		return  value;
	}
}
