package com.create.model;

/**
 * 
 * <p>
 * 标题：屏幕点对象
 * </p>
 * <p>
 * 描述：屏幕上像素点的位置
 * </p>
 * <p>
 * Copyright：Copyright(c) 2014 founder
 * </p>
 * <p>
 * 日期：2014-6-11
 * </p>
 * 
 * @author wan_song
 */
public class ScreenPoint {
	private int column;
	private int row;
	public ScreenPoint(int row,int column) {
		super();
		this.column = column;
		this.row = row;
	}
	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ScreenPoint){
			ScreenPoint other=(ScreenPoint) obj;
			if(other.getColumn()==this.getColumn()&&other.getRow()==this.getRow()){
				return true;
			}
		}
		return false;
	}
}
