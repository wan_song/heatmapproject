package com.create.model;

public interface Easing {
	public int calculate(double scale,long value);
}
