package com.create.colortable;

import java.util.Arrays;
import java.util.List;

import com.create.model.HeatMapConfig;
import com.create.model.HeatSpotMode;
import com.create.model.Reseau;
/**
 * 
 * <p>标题：颜色表</p>
 * <p>描述：</p>
 * <p>Copyright：Copyright(c) 2014 founder</p>
 * <p>日期：2014-6-12</p>
 * @author	wan_song
 */
public class CustomColorTable extends ColorTable {
	private ColorConstant colorConstant;
	private int defaultAlpha=160;
	private boolean useLocalMax;
	private HeatSpotMode mode;
	public CustomColorTable() {
		this.colorConstant=new ColorConstant();
		
	}
	@Override
	public void buildColorTable(HeatMapConfig config,HeatSpotMode mode,int maxValue,Reseau[][] reseaus) {
		this.defaultAlpha = (int)(config.getOpacity()/100.0*255);
		this.colorConstant.buildColorConstant(config.getResolution(), maxValue, config.getGradient());
		this.initColor(config.getGradient(), colorConstant);
		this.useLocalMax=config.isUseLocalMaximum();
		this.mode=mode;
	}
	private long factor=1;
	public int[] getARGB(int value)
	{
		if(factor==1&&useLocalMax){
			factor=mode.getMaxWeight()/colorConstant.getMaxConstant();
		}
		int[] argb = new int[4];
		for (int i = colorConstant.length()-1; i >=0; i--)
		{
			if (value >= colorConstant.get(i)*factor )
			{
				argb[3] = this.defaultAlpha;
				argb[0] = colors[i][0];
				argb[1] = colors[i][1];
				argb[2] = colors[i][2];
				break;
			}
		}
		return argb;
	}
	protected void initColor(List<ColorLevel> colorlist,ColorConstant colorConstant) {
		int colorsize=colorlist.size();
		int[][] colors=new int[colorConstant.length()][3];
		for(int i=0;i<colorsize;i++){
			Color max=colorlist.get(i).getColor();
			colors[(i+1)*colorConstant.colorscount]=new int[]{max.getRed(),max.getGreen(),max.getBlue()};
		}
		for(int i=0;i<colorsize;i++){
			int[] max=colors[(i+1)*colorConstant.colorscount];
			int[] min=colors[(i)*colorConstant.colorscount];
			int[] step=new int[]{(max[0]-min[0])/colorConstant.colorscount,(max[1]-min[1])/colorConstant.colorscount,(max[2]-min[2])/colorConstant.colorscount};
			for(int j=0;j<colorConstant.colorscount;j++){
				colors[i*colorConstant.colorscount+j]=new int[]{step[0]*j+min[0],step[1]*j+min[1],step[2]*j+min[2]};
			}
		}
		this.colors=colors;
		System.out.println("COLORS:"+Arrays.asList(colors));
		System.out.println("CONSTT:"+Arrays.toString(colorConstant.getColorConstant()));
	}

	int[][] colors;

}
