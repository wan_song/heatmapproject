package com.create.colortable;

public class ColorLevel {
	private String color;
	private int level;

	public ColorLevel(String color, int level) {
		super();
		this.color = color;
		this.level = level;
	}

	@Override
	public String toString() {
		return "ColorLevel [color=" + color + "]";
	}

	public Color getColor() {
		return new Color(this.color);
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
}
