package com.create.colortable;

import com.create.model.HeatMapConfig;
import com.create.model.HeatSpotMode;
import com.create.model.Reseau;

public abstract class ColorTable {

	public abstract int[] getARGB(int i);
	public abstract void buildColorTable(HeatMapConfig config,HeatSpotMode mode, int alpha,Reseau[][] reseaus);
}
