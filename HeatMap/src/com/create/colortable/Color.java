package com.create.colortable;
public class Color{
		private String color;
			public Color(String color) {
				if(color.length()<6){
					int l=color.length();
					for(int i=0;i<6-l;i++){
						color+="0";
					}
				}
				this.color=color;
				
			}

			public int getBlue() {
				
				// TODO Auto-generated method stub
				return Integer.valueOf(color.substring(4, 6),16);
			}

			public int getGreen() {
				
				// TODO Auto-generated method stub
				return Integer.valueOf(color.substring(2, 4),16);
			}


			public int getRed() {
				// TODO Auto-generated method stub
				return Integer.valueOf(color.substring(0, 2),16);
			}

			@Override
			public String toString() {
				return "Color [getBlue()=" + getBlue() + ", getGreen()="
						+ getGreen() + ", getRed()=" + getRed() + "]";
			}
			
		}