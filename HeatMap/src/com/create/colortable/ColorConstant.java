package com.create.colortable;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * <p>
 * 标题：
 * </p>
 * <p>
 * 描述：
 * </p>
 * <p>
 * Copyright：Copyright(c) 2014 founder
 * </p>
 * <p>
 * 日期：2014-6-11
 * </p>
 * 
 * @author wan_song
 */
public class ColorConstant {
	private int[] colorConstant;
	/**
	 * 每一个设定的着色值间的过度色的多少
	 */
	public final int colorscount=30;

	public int length() {
		return colorConstant.length;
	}

	public int get(int i) {
		return colorConstant[i];
	}
/**
 * 
 * <p>说明： 构建颜色权重中间组</p>
 * <p>时间：2014-6-16 上午11:09:25</p>
 * @param maxValue
 * @param colorlist
 */
	private double mins=0.0000019073486328125;
	public void buildColorConstant(double resolution,
			int maxValue,List<ColorLevel> colorlist) {
		int colorsize=colorlist.size();
		this.colorConstant=new int[colorlist.size()*this.colorscount+1];
		for(int i=0;i<colorsize;i++){
			ColorLevel cl=colorlist.get(i);
			int max=this.colorConstant[(i+1)*colorscount]=maxValue*(getLevel(mins, resolution)*cl.getLevel());
			int min=this.colorConstant[i*colorscount];
			int step=(max-min)/colorscount;
			if(step==0){
				step=1;
			}
			for(int j=0;j<colorscount;j++){
				this.colorConstant[i*colorscount+j]=(step*j+min);
			}
		}
	}
	private int getLevel(double mins, double nows){
	//	System.out.println("getLevel in");
		int level = 1;
		while(true){
			if(Math.round(nows/mins) == 1){
				break ;
			}else{
				nows = nows/2;
				level++;
			}
			if(level > 20){
				break;
			}
		}
		return level;
	}
	public int getMaxConstant(){
		return this.colorConstant[length()-1];
	}
		@Override
	public String toString() {
		return "ColorConstant [colorConstant=" + Arrays.toString(colorConstant)
				+ ", colorscount=" + colorscount + "]";
	}

	public int[] getColorConstant() {
		return colorConstant;
	}

}
