package com.create;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import com.create.model.HeatMapConfig;
import com.create.model.HeatSpotMode;
import com.create.model.HeatView;
import com.create.model.MapPoint;

/**
 * 
 * <p>标题： 创建热区图片类</p>
 * <p>描述：　创建热区图片入口</p>
 * <p>Copyright：Copyright(c) 2014 founder</p>
 * <p>日期：2014-6-11</p>
 * @author	wan_song
 */
public class HeatMap {
	private HeatView heatview;
	private HeatMapConfig config;
	private File resultDir;
	public HeatMap(HeatMapConfig heatMapConfig,File resultDir) {
		this.config=heatMapConfig;
		this.heatview=new HeatView(heatMapConfig);
		this.resultDir=resultDir;
	}
	
	public HeatMapConfig getConfig() {
		return config;
	}

	public void setConfig(HeatMapConfig config) {
		this.config = config;
		this.heatview.setConfig(config);
	}

	/**
	 * 
	 * <p>说明： 创建热区图片并返回文件生成的地址</p>
	 * <p>时间：2014-6-11 下午4:20:30</p>
	 * @return
	 */
	public String createHeatImage(List<MapPoint> points){
		int maxValue=80;
		HeatSpotMode mode=HeatSpotMode.getInstance(config.getExtent(), config.getRadius(), config.getResolution(),maxValue);
		System.out.println("开始计算权重");
		long t1=System.currentTimeMillis();
		mode.buildModel(points);
		System.out.println(mode.getWeightRange());
		System.out.println("权重计算结束,"+(System.currentTimeMillis()-t1)+"ms　　Weight:"+mode.getMaxWeight());
		System.out.println("开始绘制图形");
		BufferedImage image=heatview.createBufferImage(mode,maxValue);
		System.out.println("绘制图形结束");
		System.out.println("输出到文件");
		File file = outToFile(image);
		return file.getAbsolutePath();
	}
/**
 * 
 * <p>说明：输出到文件夹</p>
 * <p>时间：2014-6-16 下午12:13:32</p>
 * @param image
 * @return
 */
	private File outToFile(BufferedImage image) {
		if(!resultDir.exists()){
			resultDir.mkdirs();
		}
		File file=new File(resultDir, "Founder_HeatMap_"+resultDir.listFiles().length+".PNG");
		try {
			ImageIO.write(image, "PNG", new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
}
