package com.create;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import com.create.colortable.ColorLevel;
import com.create.model.Extent;
import com.create.model.HeatMapConfig;
import com.create.model.MapPoint;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

public class HeatMapTest{
	Extent extent=null;
	@Before
	public void set() {
		extent = new Extent(116.0,39.7,117.0,40.1);
		loadPointFromFile();
	}
	@Test
	public void testHeatMap() throws FileNotFoundException{
		boolean useLocalMaximum = false;
		double radius = 10;
		double resolution = (extent.getMaxX()-extent.getMinX())/1025.00;
		List<ColorLevel> gradient = new ArrayList<ColorLevel>();
		gradient.add(new ColorLevel("0000f4", 0));
		gradient.add(new ColorLevel("00ff00", 1));
		gradient.add(new ColorLevel("ff0000", 2));
		int opacity = 50;
		int threshold = 0;
		long t1=System.currentTimeMillis();
		HeatMapConfig heatMapConfig = new HeatMapConfig(extent,
				useLocalMaximum, radius, gradient, opacity, threshold,
				resolution);
	//System.setOut(new PrintStream("xx.log"));
		HeatMap heatMap = new HeatMap(heatMapConfig, new File(
				"C:/Users/wan_song/Desktop/temp"));
			heatMap.createHeatImage(mappoints);
		//System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out))); 
		System.out.println("creat time :"+(System.currentTimeMillis()-t1)+"ms");
		
	}

	public static Gson gson = null;
	public static Points point = null;
	private static List<MapPoint> mappoints = new ArrayList<MapPoint>();
	
	public static  void loadPointFromFile(){
		gson = new GsonBuilder().create();
		try {
			point = gson.fromJson(IOUtils.toString(new FileInputStream("C:\\Users\\wan_song\\sefworkspace\\heatmapproject\\HeatMap\\src_resource\\points.dat")), Points.class);
			double[][] subpoints = point.getPoints(1000);
			for (double[] p : subpoints) {
				mappoints.add(new MapPoint(p[0], p[1],(long)p[2]/10));
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void loadPoint(Extent extent){
		mappoints.clear();
		Connection conn=null;
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		    conn=DriverManager.getConnection("jdbc:oracle:thin:@172.18.68.77:1521:zjdb", "POGIS", "POGIS");
			long t1=System.currentTimeMillis();
		    Statement pstmt=conn.createStatement();
			String sql="select x,y from  T_ASJ_PT where x<" +extent.getMaxX()+" and x>"+extent.getMinX()+" and y>"+extent.getMinY()+" and y<"+extent.getMaxY();
			System.out.println(sql);
			ResultSet rs=pstmt.executeQuery(sql);
			while(rs.next()){
				mappoints.add(new MapPoint(rs.getDouble(1),rs.getDouble(2),1));
			}
			System.out.println("fetch data time:"+(System.currentTimeMillis()-t1)+"ms");
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(conn!=null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	
	public static Extent getExtent(List<MapPoint> points){
		Extent t=new Extent();
		for(MapPoint point:points){
			double x=point.getX();
			double y=point.getY();
			if(t.getMaxX()<point.getX()){
				t.setMaxX(x);
			}
			if(t.getMinX()>point.getX()){
				t.setMinX(x);
			}
			if(t.getMaxY()<point.getY()){
				t.setMaxY(y);
			}
			if(t.getMinY()>point.getY()){
				t.setMinY(y);
			}
		}
		return t;
		
	}
}
