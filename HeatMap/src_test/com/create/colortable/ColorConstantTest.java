package com.create.colortable;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

public class ColorConstantTest {
	private ColorConstant constant;
	@Before
	public void setUp() throws Exception {
		constant=new ColorConstant();
	}

	@Test
	public void testBuildColorConstant() {
		List<ColorLevel> colorList=new ArrayList<ColorLevel>();
		colorList.add(new ColorLevel("#ff0000",1));
		colorList.add(new ColorLevel("#00ff00",2));
		constant.buildColorConstant(2.123,300, colorList);
		System.out.println(constant.length());
		System.out.println(ArrayUtils.toString(constant.getColorConstant()));
	}

}
