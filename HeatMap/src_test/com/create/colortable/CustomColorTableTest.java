package com.create.colortable;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

public class CustomColorTableTest {
	private CustomColorTable cct = new CustomColorTable();
	ColorConstant constant = new ColorConstant();

	@Before
	public void setUp() throws Exception {
	}
	@Test
	public void testInitColor() {
		List<ColorLevel> colorList=new ArrayList<ColorLevel>();
		colorList.add(new ColorLevel("ff0000",1));
		colorList.add(new ColorLevel("00ff00",2));
		constant.buildColorConstant(2.123,300, colorList);
		cct.initColor(colorList,constant);
		System.out.println(ArrayUtils.toString(cct.colors));
	}

}
