/**
 * Project Name:HeatMap
 * File Name:Points.java
 * Package Name:com.jtree.data
 * Date:2014年3月25日下午8:56:29
 * Copyright (c) 2014
 *
 */

package com.create;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

/**
 * ClassName:Points <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON. <br/>
 * Date: 2014年3月25日 下午8:56:29 <br/>
 * 
 * @author Administrator
 * @version
 * @since JDK 1.6
 * @see
 */
public class Points {
	@SerializedName("points")
	private double[][] points;

	public double[][] getPoints(int num) {
		if(num<points.length)
			return Arrays.copyOfRange(points, 0, num);
		else
			return points;
	}

	public void setPoints(double[][] points) {
		this.points = points;
	}
}
